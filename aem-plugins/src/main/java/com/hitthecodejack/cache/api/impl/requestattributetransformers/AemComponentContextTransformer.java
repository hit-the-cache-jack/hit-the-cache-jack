package com.hitthecodejack.cache.api.impl.requestattributetransformers;


import com.day.cq.wcm.api.components.ComponentContext;
import com.hitthecodejack.cache.api.CachedTransformedRequestAttribute;
import com.hitthecodejack.cache.api.RequestAttributeCacheTransformer;

import org.osgi.service.component.annotations.Component;

@Component(service = RequestAttributeCacheTransformer.class, property = {"process.label=AEM Component Context "
    + "Transformer"}, immediate = true)
public class AemComponentContextTransformer implements RequestAttributeCacheTransformer {

    @Override
    public CachedTransformedRequestAttribute createCachableObject(Object requestAttribute) {
        if (requestAttribute instanceof ComponentContext) {
            return new CacheableComponentContext((ComponentContext) requestAttribute);
        }
        return null;
    }
}
