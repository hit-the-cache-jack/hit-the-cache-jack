package com.hitthecodejack.cache.api;

import com.hitthecodejack.cache.core.elements.CacheElement;

import java.util.List;

public interface CacheManagerService {

    CacheElement get(String key);

    void put(String key, CacheElement component, List<String> invalidationPaths);

    void purge();

    void clear(String key);
}
