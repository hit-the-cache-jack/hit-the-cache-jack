package com.hitthecodejack.cache.api;

public interface RequestAttributeCacheTransformer {

    Object createCachableObject(Object requestAttribute);

}
