package com.hitthecodejack.cache.core.elements;

import com.hitthecodejack.cache.api.CacheManagerService;
import com.hitthecodejack.cache.core.filters.CacheServletResponseWrapper;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CacheSnippet implements CacheElement {

    private String snippet;

    public CacheSnippet(String snippet) {
        this.snippet = snippet;
    }

    @Override
    public void processCacheElement(SlingHttpServletRequest slingRequest, SlingHttpServletResponse response,
                                    CacheManagerService cacheManager) throws IOException {
        response.getWriter().append(this.snippet);
    }

    @Override
    public List<CacheElement> getChildren() {
        return Arrays.asList(this);
    }

    @Override
    public void recreateResponseHeaders(SlingHttpServletResponse response) {
        //
    }

    @Override
    public void setResponseHeaders(CacheServletResponseWrapper response) {
//
    }

    @Override
    public List<String> getInvalidationPaths() {
        return new ArrayList<>();
    }

    public String getSnippet() {
        return this.snippet;
    }
}
