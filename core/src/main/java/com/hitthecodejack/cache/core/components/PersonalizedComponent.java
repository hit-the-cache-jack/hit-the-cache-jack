package com.hitthecodejack.cache.core.components;

import com.day.cq.wcm.api.components.IncludeOptions;
import com.hitthecodejack.cache.api.config.HitTheCacheJackConfig;
import com.hitthecodejack.cache.core.elements.CacheElement;
import com.hitthecodejack.cache.core.elements.PersonalizedCacheElement;
import com.hitthecodejack.cache.core.filters.CacheServletResponseWrapper;

import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonalizedComponent extends PermissionSensitiveComponent {

    private String snippet;


    private final Logger logger = LoggerFactory.getLogger(getClass());

    public PersonalizedComponent(String pathBasedId, String resourceToInclude, String resourceType, HitTheCacheJackConfig config) {
        super(pathBasedId, resourceToInclude, resourceType, config);
        logger.info("PersonalizedComponent path {}", pathBasedId);
    }

    @Override
    public String getSnippet() {
        return this.snippet;
    }


    @Override
    public void initializeComponent(SlingHttpServletRequest request) {

        //TODO: includeoptions and componentcontext are AEM specific, we should store and restore all request attributes
        //complexity is that request attribute may contain session-specific objects such as resources and resource
        // resolvers
        this.cacheElement = new PersonalizedCacheElement(this.pathBasedId, this.resourceToInclude, request.getResource().getResourceType(), null,
            IncludeOptions.getOptions(request, true), getCacheableRequestAttributes(request), this.getInvalidationPaths());
    }

    @Override
    public void done(CacheServletResponseWrapper cacheResponseWrapper) {
        this.cacheElement.setResponseHeaders(cacheResponseWrapper);
        this.snippet = cacheResponseWrapper.getResponseString();
    }

    @Override
    void createCacheElement(List<CacheElement> childElements, CacheServletResponseWrapper response) {
        //nothing to do
    }

    @Override
    public boolean isCacheable() {
        return false;
    }
}
