package com.hitthecodejack.cache.api.impl.requestattributetransformers;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.api.components.ComponentManager;
import com.hitthecodejack.cache.api.CachedTransformedRequestAttribute;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

//TODO: this class is AEM specific. It should be delivered in a separate bundle
public class CacheableComponentContext implements CachedTransformedRequestAttribute {

    private ComponentContext incompleteComponentContext;
    private String resourcePath;
    private String pagePath;

    public CacheableComponentContext(ComponentContext originalComponentContext) {
        this.incompleteComponentContext = originalComponentContext;
        this.resourcePath = originalComponentContext.getResource().getPath();
        if (originalComponentContext.getPage() != null) {
            this.pagePath = originalComponentContext.getPage().getPath();
        }
    }


    @Override
    public Object recreateRequestAttribute(SlingHttpServletRequest request) {
        Resource restoredResource = request.getResourceResolver().getResource(resourcePath);
        restoreContext(request, this.incompleteComponentContext, restoredResource);
        return incompleteComponentContext;
    }

    public void restoreContext(SlingHttpServletRequest request, ComponentContext context, Resource restoredResource) {
        ComponentManager compMgr =
            request.getResourceResolver().adaptTo(ComponentManager.class);

        try {
            FieldUtils.writeField(context, "resource", restoredResource, true);
            FieldUtils.writeField(context, "component",
                compMgr.getComponentOfResource(restoredResource), true);
            if (FieldUtils.getField(context.getClass(), "page", true) != null) {
                Resource pageResource = request.getResourceResolver().getResource(this.pagePath);
                if (pageResource != null) {
                    Page page = pageResource.adaptTo(Page.class);
                    FieldUtils.writeField(context, "page", page, true);
                }
            }
            if (context.getParent() != null && restoredResource.getParent() != null) {
                restoreContext(request, context.getParent(), restoredResource.getParent());
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
