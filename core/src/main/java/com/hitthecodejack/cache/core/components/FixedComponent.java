package com.hitthecodejack.cache.core.components;

import com.hitthecodejack.cache.api.CacheManagerService;
import com.hitthecodejack.cache.core.filters.CacheServletResponseWrapper;

public class FixedComponent extends CachedComponent {


    private CacheManagerService cacheManager;
    public FixedComponent(String pathBasedId, String resourcePath, CacheManagerService cacheManager) {
        super(pathBasedId, resourcePath);
        this.cacheManager = cacheManager;
    }

    @Override
    public void done(CacheServletResponseWrapper response) {
        super.done(response);
        this.cacheManager.put(this.getComponentId(), this.getCacheElement(), this.getInvalidationPaths());
    }

    @Override
    public boolean canBeAgreggated() {
        return false;
    }
}
