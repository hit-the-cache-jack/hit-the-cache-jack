package com.hitthecodejack.cache.api;

import org.apache.sling.api.SlingHttpServletRequest;

public interface CachedTransformedRequestAttribute {

    Object recreateRequestAttribute(SlingHttpServletRequest request);
}
