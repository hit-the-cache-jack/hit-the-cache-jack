package com.hitthecodejack.cache.core.components;

import com.hitthecodejack.cache.core.elements.AggregateCacheElement;
import com.hitthecodejack.cache.core.elements.CacheElement;
import com.hitthecodejack.cache.core.elements.CacheSnippet;
import com.hitthecodejack.cache.core.elements.CachedStream;
import com.hitthecodejack.cache.core.elements.helpers.ResponseSplitterState;
import com.hitthecodejack.cache.core.filters.CacheServletResponseWrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CachedComponent {

    String pathBasedId;
    private String resourcePath;
    private List<CachedComponent> childComponents = new ArrayList<>();
    private List<String> invalidationPaths;
    private String responseComplete = "";
    CacheElement cacheElement;


    private final Logger logger = LoggerFactory.getLogger(getClass());

    public CachedComponent(String pathBasedId, String resourcePath) {
        this.pathBasedId = pathBasedId;
        this.resourcePath = resourcePath;
        this.invalidationPaths = new ArrayList<>();
        this.invalidationPaths.add(resourcePath);
    }


    public String getSnippet() {
        return this.responseComplete;
    }


    public void done(CacheServletResponseWrapper response) {
        List<CacheElement> childElements = new ArrayList<>();
        if (response.isWriterInitialized()) {
            childElements = splitResponseIntoSnippets(response.getResponseString());
        } else if (response.isBaosUsed()) {
            childElements.add(new CachedStream(response.getCachedStream(), this.invalidationPaths));
        }
        this.createCacheElement(childElements, response);
    }

    void createCacheElement(List<CacheElement> childElements, CacheServletResponseWrapper response) {
        this.cacheElement = new AggregateCacheElement(pathBasedId, childElements, response, invalidationPaths);
    }

    public List<CachedComponent> getChildren() {
        return this.childComponents;
    }

    public boolean canBeAgreggated() {
        for (CachedComponent child : this.childComponents) {
            if (!child.canBeAgreggated()) {
                return false;
            }
        }
        return true;
    }


    public boolean isCacheable() {
        return true;
    }

    public void initializeComponent(SlingHttpServletRequest request) {
        //default cache component requires no perpare
    }


    public void addChildComponent(CachedComponent childComponent) {
        this.childComponents.add(childComponent);
    }

    public CacheElement getCacheElement() {
        return this.cacheElement;
    }


    public String getComponentId() {
        return this.pathBasedId;
    }

    private List<CacheElement> splitResponseIntoSnippets(String response) {

        //the component is fully processed, which leaves us with the full html and all the child components
        this.responseComplete = response;

        if (this.canBeAgreggated()) {
            logger.debug("no need to split component as is does not contain any non-aggeggable elements  {}",
                this.pathBasedId);
            return Arrays.asList(new CacheSnippet(this.responseComplete));
        }

        //go through all child elements and split the full html to get a list of snippets and child elements
        ResponseSplitterState state = new ResponseSplitterState();

        for (CachedComponent childComponent : this.childComponents) {
            String fullChildSnippet = childComponent.getSnippet();
            processStartOfChildElement(state, fullChildSnippet);
            if (childComponent.canBeAgreggated()) {
                aggregateChildrenOfChild(state, childComponent);
            } else {
                state.combineSnippets();
                state.addChild(childComponent.getCacheElement());
            }
        }
        if (state.getSnippetIndex() > this.responseComplete.length()) {
            logger.error("PROBLEM with sniipet 2 {}", this.pathBasedId);
            state.setError();
        } else {
            state.addCombinableSnippet(new CacheSnippet(this.responseComplete.substring(state.getSnippetIndex())));
        }
        state.combineSnippets();
        if (state.isError()) {
            logger.error("Failed to split component {} ", this.pathBasedId);
            return Arrays.asList(new CacheSnippet(this.responseComplete));
        } else {
            return state.getSubCacheElements();
        }
    }

    private void aggregateChildrenOfChild(ResponseSplitterState state, CachedComponent childComponent) {
        for (CacheElement childOfChild : childComponent.getCacheElement().getChildren()) {
            if (childOfChild instanceof CacheSnippet) {
                state.addCombinableSnippet((CacheSnippet) childOfChild);
            } else {
                state.combineSnippets();
                state.addChild(childOfChild);
            }
        }
    }

    private void processStartOfChildElement(ResponseSplitterState state, String fullChildSnippet) {
        int snippetEndIndex = responseComplete.indexOf(fullChildSnippet, state.getSnippetIndex());
        if (snippetEndIndex == -1) {
            logger.error("PROBLEM with sniipet {}", this.pathBasedId);
            state.setError();
        } else {
            state.addCombinableSnippet(new CacheSnippet(this.responseComplete.substring(state.getSnippetIndex(),
                snippetEndIndex)));
            state.setSnippetIndex(snippetEndIndex + fullChildSnippet.length());
        }

    }

    public List<String> getInvalidationPaths() {
        return invalidationPaths;
    }

    public void addInvalidationPaths(List<String> invalidationPaths) {
        if (invalidationPaths == null) {
            return;
        }
        for (String invalidationPath : invalidationPaths) {
            //TODO if search for the first dot in URI is valid... alternative would be to resolve the resource
            if (!StringUtils.startsWith(invalidationPath,  StringUtils.substringBefore(this.resourcePath, "."))) {
                this.invalidationPaths.add(invalidationPath);
            }
        }
    }
}
