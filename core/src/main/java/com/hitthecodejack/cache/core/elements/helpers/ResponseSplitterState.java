package com.hitthecodejack.cache.core.elements.helpers;

import com.hitthecodejack.cache.core.elements.CacheElement;
import com.hitthecodejack.cache.core.elements.CacheSnippet;

import java.util.ArrayList;
import java.util.List;

public class ResponseSplitterState {

    private int snippetIndex = 0;
    private List<CacheSnippet> combinableSnippets = new ArrayList<>();

    private List<CacheElement> subCacheElements = new ArrayList<>();
    private boolean error = false;

    public int getSnippetIndex() {
        return snippetIndex;
    }

    public void setSnippetIndex(int snippetIndex) {
        this.snippetIndex = snippetIndex;
    }

    public List<CacheSnippet> getCombinableSnippets() {
        return combinableSnippets;
    }

    public void addCombinableSnippet(CacheSnippet combinableSnippet) {
        this.combinableSnippets.add(combinableSnippet);
    }

    public List<CacheElement> getSubCacheElements() {
        return subCacheElements;
    }

    public void addChild(CacheElement newChild) {
        this.subCacheElements.add(newChild);
    }

    public boolean isError() {
        return error;
    }

    public void setError() {
        this.error = true;
    }


    public void combineSnippets() {
        if (!combinableSnippets.isEmpty()) {
            StringBuilder snippetBuilder = new StringBuilder();
            for(CacheSnippet snippet : combinableSnippets){
                snippetBuilder.append(snippet.getSnippet());
            }
            CacheSnippet combinedSnippet = new CacheSnippet(snippetBuilder.toString());
            subCacheElements.add(combinedSnippet);
        }
        this.combinableSnippets = new ArrayList<>();
    }
}
