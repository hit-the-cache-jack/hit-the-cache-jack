package com.hitthecodejack.cache.core.listeners;

import com.day.cq.replication.ReplicationEvent;
import com.hitthecodejack.cache.api.CacheManagerService;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.*;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;

@Component(service = EventHandler.class, configurationPolicy = ConfigurationPolicy.OPTIONAL, property = {
    "event.topics=com/adobe/granite/replication"}, immediate = true)
public class PublishEventCachePurgeListener implements EventHandler {

    private Session adminSession;

    private transient final Logger logger = LoggerFactory.getLogger(getClass());

    @Reference
    org.apache.sling.jcr.api.SlingRepository repository;
    @Reference
    CacheManagerService cacheManager;

    @Activate
    public void activate(ComponentContext context) throws Exception {
        logger.info("activating ExampleObservation");

    }

    @Deactivate
    public void deactivate() {
        if (adminSession != null) {
            adminSession.logout();
        }
    }

    @Override
    public void handleEvent(Event event) {
        String n[] = event.getPropertyNames();
        ReplicationEvent action = ReplicationEvent.fromEvent(event);

        if (action != null) {

            logger.info("Replication action {} occured on {} ", action.getReplicationAction().getType(),
                action.getReplicationAction().getPath());
            cacheManager.clear(action.getReplicationAction().getPath());
        }
    }
}
