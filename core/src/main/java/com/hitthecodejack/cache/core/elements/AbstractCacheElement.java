package com.hitthecodejack.cache.core.elements;

import com.hitthecodejack.cache.core.filters.CacheServletResponseWrapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.SlingHttpServletResponse;

public abstract class AbstractCacheElement implements CacheElement {


    Map<String, List<String>> responseHeaders;
    private List<String> invalidationPaths;
    String elementId;

    public AbstractCacheElement(String elementId, CacheServletResponseWrapper response, List<String> invalidationPaths) {
        this.elementId = elementId;
        this.invalidationPaths = invalidationPaths;
        if (response != null) {
            this.responseHeaders = response.getHeaderMap();
        } else {
            this.responseHeaders = new HashMap<>();
        }
    }

    @Override
    public void setResponseHeaders(CacheServletResponseWrapper response) {
        this.responseHeaders = response.getHeaderMap();
    }


    @Override
    public void recreateResponseHeaders(SlingHttpServletResponse response) {
        response.setHeader("hitthecache", "jack");
        if (this.responseHeaders == null) {
            return;
        }
        for (Map.Entry<String, List<String>> entry : this.responseHeaders.entrySet()) {
            //TODO do we need to support multiple?
            response.setHeader(entry.getKey(), entry.getValue().get(0));
        }
    }

    String getElementId() {
        return elementId;
    }

    @Override
    public List<String> getInvalidationPaths() {
        return invalidationPaths;
    }
}
