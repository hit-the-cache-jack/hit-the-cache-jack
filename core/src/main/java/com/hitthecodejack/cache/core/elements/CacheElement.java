package com.hitthecodejack.cache.core.elements;

import com.hitthecodejack.cache.api.CacheManagerService;
import com.hitthecodejack.cache.core.filters.CacheServletResponseWrapper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;

public interface CacheElement {

    void processCacheElement(SlingHttpServletRequest slingRequest, SlingHttpServletResponse response,
                             CacheManagerService cacheManager) throws IOException, ServletException;

    List<CacheElement> getChildren();

    void recreateResponseHeaders(SlingHttpServletResponse response);

    void setResponseHeaders(CacheServletResponseWrapper response);

    List<String> getInvalidationPaths();
}
