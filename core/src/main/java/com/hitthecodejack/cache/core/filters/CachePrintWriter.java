package com.hitthecodejack.cache.core.filters;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class CachePrintWriter extends Writer {
    private List<Writer> delegates;

    public CachePrintWriter(List<Writer> delegates){
        this.delegates = delegates;
    }

    public void write(char cbuf[], int off, int len) throws IOException {
        for(Writer w: delegates){
            w.write(cbuf, off, len);
        }
    }

    @Override
    public void flush() throws IOException {
        for(Writer w: delegates){
            w.flush();
        }
    }

    @Override
    public void close() throws IOException {
        for(Writer w: delegates){
            w.close();
        }
    }


}
