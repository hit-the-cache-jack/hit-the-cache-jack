package com.hitthecodejack.cache.core.components;

import com.day.cq.wcm.api.components.IncludeOptions;
import com.hitthecodejack.cache.api.CacheManagerService;
import com.hitthecodejack.cache.api.config.HitTheCacheJackConfig;
import com.hitthecodejack.cache.core.elements.AggregateCacheElement;
import com.hitthecodejack.cache.core.elements.CacheElement;
import com.hitthecodejack.cache.core.elements.GroupCacheElement;
import com.hitthecodejack.cache.core.filters.CacheServletResponseWrapper;

import java.util.Iterator;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GroupComponent extends PermissionSensitiveComponent {
    private String groupId;
    private CacheManagerService cacheManager;
    private CacheElement groupSepecificCacheElement;


    private final Logger logger = LoggerFactory.getLogger(getClass());

    public GroupComponent(String pathBasedId, String resourceToInclude, SlingHttpServletRequest request,
                          CacheManagerService cacheManager,
                          HitTheCacheJackConfig config) {
        this(pathBasedId, resourceToInclude, request.getResource().getResourceType(), request, cacheManager, config);
    }

    public GroupComponent(String pathBasedId, String resourceToInclude, String resourceType, SlingHttpServletRequest request,
                          CacheManagerService cacheManager, HitTheCacheJackConfig config) {
        super(pathBasedId, resourceToInclude, resourceType, config);
        this.groupId = getGroupId(request);
        this.cacheManager = cacheManager;
    }

    @Override
    public void initializeComponent(SlingHttpServletRequest request) {

        //TODO: includeoptions are AEM specific, we should store and restore all request attributes
        //complexity is that request attribute may contain session-specific objects such as resources and resource
        // resolvers
        this.cacheElement = new GroupCacheElement(pathBasedId, resourceToInclude, resourceType,
            IncludeOptions.getOptions(request, true),
            getCacheableRequestAttributes(request), null,
            this.getInvalidationPaths());
    }

    private String getGroupId(SlingHttpServletRequest request) {
        Session session = request.getResourceResolver().adaptTo(Session.class);
        String userId = session != null ? session.getUserID() : "";
        if (StringUtils.equals(userId, "anonymous")) {
            return "anonymous";
        }
        UserManager userManager = request.getResourceResolver().adaptTo(UserManager.class);
        StringBuilder groupIdBuilder = new StringBuilder();
        try {
            Iterator<Group> groups = userManager.getAuthorizable(userId).memberOf();
            while (groups.hasNext()) {
                groupIdBuilder.append(";");
                groupIdBuilder.append(groups.next().getID());
            }
        } catch (RepositoryException e) {
            logger.warn("failed to generate group id {}", e);
        }
        return groupIdBuilder.toString();
    }

    @Override
    public void done(CacheServletResponseWrapper response) {
        super.done(response);
        this.cacheManager.put(this.getComponentId(), this.groupSepecificCacheElement, this.getInvalidationPaths());
        logger.info("--- put the cached value for group component {} ", this.getComponentId());
    }

    @Override
    public String getComponentId() {
        return this.pathBasedId + this.groupId + this.resourceType;
    }


    @Override
    void createCacheElement(List<CacheElement> childElements, CacheServletResponseWrapper response) {
        this.groupSepecificCacheElement = new AggregateCacheElement(this.getComponentId(), childElements, response,
            this.getInvalidationPaths());
    }

    @Override
    public boolean isCacheable() {
        //we do not want to cache the group component directly... caching is handled in the "done" method
        return false;
    }
}
