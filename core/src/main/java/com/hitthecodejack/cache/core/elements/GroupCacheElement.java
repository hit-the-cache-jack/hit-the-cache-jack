package com.hitthecodejack.cache.core.elements;

import com.day.cq.wcm.api.components.IncludeOptions;
import com.hitthecodejack.cache.api.CacheManagerService;
import com.hitthecodejack.cache.core.components.CachedComponent;
import com.hitthecodejack.cache.core.components.GroupComponent;
import com.hitthecodejack.cache.core.filters.CacheServletResponseWrapper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GroupCacheElement extends PersonalizedCacheElement implements CacheElement {


    private final Logger logger = LoggerFactory.getLogger(getClass());

    public GroupCacheElement(String elementId, String resourceToInclude, String resourceType, IncludeOptions options,
                             Map<String, Object> cacheableRequestAttributes, CacheServletResponseWrapper response,
                             List<String> invalidationPaths) {
        super(elementId, resourceToInclude, resourceType, response, options, cacheableRequestAttributes,
            invalidationPaths);
    }


    @Override
    public void processCacheElement(SlingHttpServletRequest slingRequest, SlingHttpServletResponse response,
                                    CacheManagerService cacheManager) throws IOException, ServletException {

        logger.debug("process group component {}", this.getElementId());
        CachedComponent cacheComponentInfo = new GroupComponent(this.elementId, this.path, this.resourceType, slingRequest,
            cacheManager,null);
        CacheElement cachedElement = cacheManager.get(cacheComponentInfo.getComponentId());
        if (cachedElement != null && slingRequest.getHeader("ForceCreateCache") == null) {
            logger.debug("rendering group component {} from cache", this.getElementId());
            for (CacheElement element : cachedElement.getChildren()) {
                element.processCacheElement(slingRequest, response, cacheManager);
            }
            return;
        }
        super.processCacheElement(slingRequest, response, cacheManager);
    }


}
