package com.hitthecodejack.cache.core.filters;

import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.wrappers.SlingHttpServletResponseWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CacheServletResponseWrapper extends SlingHttpServletResponseWrapper implements Serializable {

    private transient StringWriter buffer;
    private transient PrintWriter printWriter;

    private transient ByteArrayOutputStream baos = new ByteArrayOutputStream();
    private boolean isBaosUsed = false;
    private boolean isWriterInitialized = false;

    private int status = 200;
    private Integer contentLength = null;

    private Map<String, List<String>> headerMap = new HashMap<>();

    private ServletOutputStream stream = initServletOutputStream();

    private ServletOutputStream initServletOutputStream() {
        return new ServletOutputStream() {
            @Override
            public void write(int b) throws IOException {
                isBaosUsed = true;
                baos.write(b);
            }

        };
    }

    /**
     * Constructs a response adaptor wrapping the given response.
     *
     * @throws IllegalArgumentException if the response is null
     */
    public CacheServletResponseWrapper(SlingHttpServletResponse response) throws IOException {
        super(response);
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        return stream;
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        initBuffer(false);
        return printWriter;
    }

    /**
     * returns the Response String of the Wrapping Response
     *
     * @return the Response String of the Wrapping Response
     */
    public String getResponseString() {
        if (isWriterInitialized) {
            return this.buffer.toString();
        }
        if (isBaosUsed) {
            try {
                return this.baos.toString("UTF-8");
            } catch (UnsupportedEncodingException e) {
                //TODO
                e.printStackTrace();
            }
        }
        return "";
    }

    @Override
    public void setHeader(String name, String value) {
        super.setHeader(name, value);
        List<String> headerList = new ArrayList<>();
        headerList.add(value);
        this.headerMap.put(name, headerList);
    }

    @Override
    public void addHeader(String name, String value) {
        super.addHeader(name, value);
        List<String> headerList = this.headerMap.get(name);
        headerList = (headerList == null) ? new ArrayList<>() : headerList;
        headerList.add(value);
        this.headerMap.put(name, headerList);
    }

    @Override
    public void setDateHeader(String name, long date) {
        super.setDateHeader(name, date);
    }

    @Override
    public void addDateHeader(String name, long date) {
        super.addDateHeader(name, date);
    }

    @Override
    public void setIntHeader(String name, int value) {
        super.setIntHeader(name, value);
    }

    @Override
    public void addIntHeader(String name, int value) {
        super.addIntHeader(name, value);
    }

    public List<String> getHeaders(String name) {
        return headerMap.containsKey(name) ? headerMap.get(name) : new ArrayList<>();
    }

    public Map<String, List<String>> getHeaderMap() {
        return headerMap;
    }

    public void promoteToOriginalResponse() throws IOException {
        //this ensures that the parent response is updated with all content of the stream or the writer.
        //TODO: not sure if this is can be removed
        if (isWriterInitialized && buffer.getBuffer().length() > 0) {
            getResponse().getWriter().flush();
        }
        if (isBaosUsed) {
            getResponse().getOutputStream().write(baos.toByteArray());
            getResponse().getOutputStream().flush();

        }
    }

    @Override
    public void setStatus(int sc, String sm) {
        this.status = sc;
        super.setStatus(sc, sm);
    }

    public int getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(int sc) {
        this.status = sc;
        super.setStatus(sc);
    }

    @Override
    public int getBufferSize() {
        return 0;
    }

    @Override
    public void flushBuffer() throws IOException {
        if (isWriterInitialized) {
            printWriter.flush();
        }
    }

    @Override
    public void reset() {
        super.reset();
        headerMap = new HashMap<>();
        initBuffer(true);
    }

    @Override
    public void resetBuffer() {
        super.resetBuffer();
        initBuffer(true);
    }

    @Override
    public void sendError(int sc, String msg) throws IOException {
        this.status = sc;
        super.sendError(sc, msg);
    }

    @Override
    public void sendError(int sc) throws IOException {
        this.status = sc;
        super.sendError(sc);
    }

    private void initBuffer(boolean forceInit) {
        if (!forceInit && isWriterInitialized) {
            return;
        }
        if (isBaosUsed) {
            return;
        }
        this.buffer = new StringWriter();
        List<Writer> writers = new ArrayList<>();
        try {
            writers.add(getResponse().getWriter());
        } catch (IOException e) {
            //TODO
            e.printStackTrace();
        }
        writers.add(this.buffer);
        this.printWriter = new PrintWriter(new CachePrintWriter(writers));
        isWriterInitialized = true;
    }

    public boolean isBaosUsed() {
        return isBaosUsed;
    }

    public boolean isWriterInitialized() {
        return isWriterInitialized;
    }

    public byte[] getCachedStream() {
        return this.baos.toByteArray();
    }

    public Integer getContentLength() {
        return contentLength;
    }

    @Override
    public void setContentLength(int len) {
        this.contentLength = len;
        super.setContentLength(len);
    }
}
