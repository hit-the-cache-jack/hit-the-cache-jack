package com.hitthecodejack.cache.core.elements;

import com.hitthecodejack.cache.api.CacheManagerService;
import com.hitthecodejack.cache.core.filters.CacheServletResponseWrapper;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AggregateCacheElement extends AbstractCacheElement implements CacheElement {


    private List<CacheElement> subElements;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public AggregateCacheElement(String path, List<CacheElement> subElements, CacheServletResponseWrapper response,
                                 List<String> invalidationPaths) {
        super(path, response, invalidationPaths);
        this.subElements = subElements;
    }


    @Override
    public void processCacheElement(SlingHttpServletRequest slingRequest, SlingHttpServletResponse response,
                                    CacheManagerService cacheManager) throws IOException, ServletException {
        logger.info("rendering cached component {}", this.getElementId());

        for (int i = 0; i<this.subElements.size(); i++) {
            this.subElements.get(i).processCacheElement(slingRequest, response, cacheManager);
        }
    }

    @Override
    public List<CacheElement> getChildren() {
        return this.subElements;
    }
}
