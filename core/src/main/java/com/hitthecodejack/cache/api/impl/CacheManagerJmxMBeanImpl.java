package com.hitthecodejack.cache.api.impl;

import com.adobe.granite.jmx.annotation.AnnotatedStandardMBean;
import com.hitthecodejack.cache.api.CacheManagerJmxMBean;
import com.hitthecodejack.cache.api.CacheManagerService;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.management.DynamicMBean;
import javax.management.NotCompliantMBeanException;

/**
 * class to enable clearing the cache via jmx console
 */
@Component(service = DynamicMBean.class, property = {"jmx.objectname=hit the cache jack cache manager:type=Service,name=cacheManagerJmx"}, immediate = true)
public class CacheManagerJmxMBeanImpl extends AnnotatedStandardMBean implements CacheManagerJmxMBean {

    @Reference
    CacheManagerService cacheManager;

    public CacheManagerJmxMBeanImpl() throws NotCompliantMBeanException {
        super(CacheManagerJmxMBean.class);
    }

    @Override
    public void purgeCache(){
        cacheManager.purge();
    }

    @Override
    public void clear(String path){
        cacheManager.clear(path);
    }



}
