package com.hitthecodejack.cache.core;


import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_EXTENSIONS;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_METHODS;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_SELECTORS;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;

@Component(
    service = { Servlet.class },
    property = {
        SLING_SERVLET_METHODS + "=GET",
        SLING_SERVLET_EXTENSIONS + "=json",
        SLING_SERVLET_SELECTORS + "=teststream",
        SLING_SERVLET_SELECTORS + "=testzip",
        SLING_SERVLET_RESOURCE_TYPES +"=sling/servlet/default",
    }
)
public class TestServlet extends SlingSafeMethodsServlet {

    @Override
    protected void doGet(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) throws ServletException, IOException {

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("value", "Es gibt im Moment in diese Mannschaft, oh, einige Spieler vergessen ihnen Profi, was sie sind. Ich lese nicht sehr viele Zeitungen, aber ich habe gehört viele Situation. Erstens: Wir haben nicht offensiv gespielt. Es gibt keine deutsche Mannschaft spiel offensiv oh dynam-offensiv wie Bayern. Letzte Spiel hatten wir in Platz drei Spitzen: Elber, Jancker und dann Zickler. Wir mussen nicht vergessen Zickler. Zickler ist eine Spitzen mehr Mehmet e mehr Basler. Ist klar diese Wörter, is möglich verstehen, was ich hab' gesagt? Dann. Offensiv, offensiv ist wie maken wir in Platz.");
        resultMap.put("trap","\n"
                + "\n"
                + "Zweite: Ich habe erklärt mit diese zwei Spieler: Nach Dortmund brauchen vielleicht Halbzeitpause. "
                + "Ich habe auch andere Mannschaften gesehen in Europa nach diese Mittwoch. Ich habe gesehen auch "
                + "zwei Tage die Training. Ein Trainer nicht ein Idiot! Ein Trainer seh, was passieren im Platz. In "
                + "diese Spiel wie zwei, drei oder vier Spieler, die waren schwach wie eine Flasche leer!\n"
                + "\"Was erlauben Strunz?\"\n"
                + "\n"
                + "Haben Sie gesehen Mittwoch, welche Mannschaft hat gespielt Mittwoch? Hat gespielt Mehmet, oder "
                + "gespielt Basler, oder gespielt Trapattoni? Diese Spieler beklagen mehr als spiel! Wissen Sie, "
                + "warum die Italien-Mannschaften kaufen nicht diese Spieler? Weil wir haben gesehen viele Male Summe"
                + " Spiel. Haben gesagt, sind nicht Spieler für die italienisch Meisters.\n"
                + "\n"
                + "Strunz! Strunz ist zwei Jahre hier, hat gespielt zehn Spiele, ist immer verletzt. Was erlauben "
                + "Strunz? Letzte Jahre Meister geworden mit Hamann äh Nerlinger. Diese Spieler waren Spieler! War "
                + "Meister geworden! Ist immer verletzt! Hat gespielt 25 Spiele in diese Mannschaft, in diese Verein!"
                + " Muss respektieren die andere Kollega! Haben viel nette Kollegan, stellen sie die Kollega in "
                + "Frage! Haben keinen Mut an Worten, aber ich weiss, was denken über diese Spieler!\n"
                + "\n"
                + "Mussen zeigen jetzt, ich will, Samstag, diese Spieler mussen zeigen mich eh seine Fans, mussen "
                + "alleine die Spiel gewinnen. Mussen alleine Spiel gewinnen. Ich bin müde jetzt Vater diese Spieler,"
                + " eh, verteidige immer diese Spieler! Ich habe immer die Schulde über diese Spieler. Einer ist "
                + "Mario, einer, ein anderer ist Mehmet! Strunz dagegen egal, hat nur gespielt 25 Prozent diese Spiel!");
        resultMap.put("over", "Ich habe fertig!");

        slingResponse.setContentType("application/json");
        String resultJson = new Gson().toJson(resultMap);
        if(slingRequest.getRequestPathInfo().getSelectorString().contains("teststream")){
            if(slingRequest.getRequestPathInfo().getSelectorString().contains("testzip")){
                OutputStream stream = new GZIPOutputStream(slingResponse.getOutputStream(), true);
                stream.write(resultJson.getBytes());
                slingResponse.addHeader("Content-Encoding", "gzip");
                stream.flush();
            } else {
                slingResponse.getOutputStream().write(resultJson.getBytes());
            }
        } else {
            slingResponse.getWriter().print(resultJson);
            slingResponse.getWriter().close();
        }
    }
}
