package com.hitthecodejack.cache.api.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@Component( immediate = true, service = HitTheCacheJackConfig.class )
@Designate(
        ocd = HitTheCacheJackConfig.Configuration.class
)
public class HitTheCacheJackConfig {


    private static final String CONTENT_TYPES = "Cached Content Types";
    private static final String IGNORE_REQUEST_ATTRIBUTES = "Ignored Request Attributes";
    private static final String IGNORED_REQUEST_PARAMS = "ignored request params";
    private static final String CACHEABLE_REQUEST_PARAMS = "cacheable Requests parameters";

    private Map<String, String> contentTypes = new HashMap<>();
    private List<String> ignoreRequestAttributes = new ArrayList<>();
    private List<String> ignoredRequestParams = new ArrayList<>();
    private List<String> cacheableRequestParams = new ArrayList<>();

    @Activate
    protected void activate( Configuration config ) {
        Map<String, String> newContentTypes = new HashMap<>();
        for(String contentType: config.cachedContentTypes()){
            String[] contentTypeElements = contentType.split(":");
            if(contentTypeElements.length== 2){
                newContentTypes.put(contentTypeElements[0], contentTypeElements[1]);
            }
        }
        this.contentTypes = newContentTypes;
        this.ignoreRequestAttributes = Arrays.asList(config.ignoreRequestAttributes());
        this.ignoredRequestParams = Arrays.asList(config.ignoredRequestParams());
        this.cacheableRequestParams = Arrays.asList(config.cacheableRequestParams());
    }

    public Map<String, String> getContentTypes() {
        return this.contentTypes;
    }

    public List<String> getIgnoreRequestAttributes() {
        return ignoreRequestAttributes;
    }

    public List<String> getIgnoredRequestParams() {
        return ignoredRequestParams;
    }

    public List<String> getCacheableRequestParams() {
        return cacheableRequestParams;
    }

    @ObjectClassDefinition(name = "Hit The Cache Jack Configuration")
    public @interface Configuration {
        @AttributeDefinition(
            name = HitTheCacheJackConfig.CONTENT_TYPES,
            description = "Content types, each entry should contain the extension, a colon and the mime type for the extension. Emample: 'html:text/html':",
            type = AttributeType.STRING
        )
        String[] cachedContentTypes() default {"html:text/html", "json:application/json", "js:application/javascript", "clientlibs:application/javascript"};

        @AttributeDefinition(
            name = HitTheCacheJackConfig.IGNORED_REQUEST_PARAMS,
            description = "Known request parameters that are ignored for page rendering and can be ignored for the caching.",
            type = AttributeType.STRING
        )
        String[] ignoredRequestParams() default {};


        @AttributeDefinition(
            name = HitTheCacheJackConfig.CACHEABLE_REQUEST_PARAMS,
            description = "request parameters that produce cacheable requests",
            type = AttributeType.STRING
        )
        String[] cacheableRequestParams() default {"wcmmode" };

        @AttributeDefinition(
            name = HitTheCacheJackConfig.IGNORE_REQUEST_ATTRIBUTES,
            description = "The specified list of request attributes is not recreated for personalized snippets.",
            type = AttributeType.STRING
        )
        String[] ignoreRequestAttributes() default {"com.adobe.cq.WCMDeveloperModeFilter.context","org.apache.sling.models.impl.ModelAdapterFactory.RealRequest", "com.day.crx.security.token.TokenCookie$Info", "org.osgi.service.http.authentication.remote.user","org.osgi.service.http.authentication.type", "cacheComponent","com.day.crx.security.token.TokenCookie","org.apache.sling.engine.impl.parameters.ParameterSupport","com.day.cq.wcm.api.components.IncludeOptions","com.day.cq.wcm.api.WCMMode","com.day.cq.wcm.api.AuthoringUIMode", "org.apache.sling.auth.core.ResourceResolver", "com.day.cq.rewriter.linkchecker.LinkCheckerSettings", "org.apache.sling.api.include.request_path_info", "org.apache.sling.api.scripting.SlingBindings", "org.apache.sling.api.include.servlet", "com.day.cq.wcm.scripting.impl.WCMBindingsValuesProvider:pageCache" };

    }

}
