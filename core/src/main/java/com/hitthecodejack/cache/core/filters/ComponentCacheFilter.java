package com.hitthecodejack.cache.core.filters;

import com.hitthecodejack.cache.api.CacheManagerService;
import com.hitthecodejack.cache.api.config.HitTheCacheJackConfig;
import com.hitthecodejack.cache.core.components.CachedComponent;
import com.hitthecodejack.cache.core.components.FixedComponent;
import com.hitthecodejack.cache.core.components.GroupComponent;
import com.hitthecodejack.cache.core.components.PersonalizedComponent;
import com.hitthecodejack.cache.core.elements.CacheElement;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.EngineConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = Filter.class,
    property = {
        Constants.SERVICE_DESCRIPTION + "=Filter to cache get responses",
        EngineConstants.SLING_FILTER_SCOPE + "=" + EngineConstants.FILTER_SCOPE_COMPONENT,
        Constants.SERVICE_RANKING + ":Integer=" + Integer.MAX_VALUE

    })
public class ComponentCacheFilter implements Filter {

    public static final String REQ_ATTR_CACHE_COMPONENT = "cacheComponent";
    @Reference
    private CacheManagerService cacheManagerService;
    @Reference
    private HitTheCacheJackConfig hitTheCacheJackConfig;
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        if (!(request instanceof SlingHttpServletRequest) || !(response instanceof SlingHttpServletResponse)) {
            filterChain.doFilter(request, response);
            return;
        }

        final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
        final SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;
        if (!this.shouldHandle(slingRequest)) {
            filterChain.doFilter(request, response);
            return;
        }

        CachedComponent cacheComponent = createCacheComponent(slingRequest);
        logger.debug("processing component with id {}", cacheComponent.getComponentId());

        //check if we already have the component in cache
        CacheElement cachedElement = cacheManagerService.get(cacheComponent.getComponentId());
        if (cachedElement != null && slingRequest.getHeader("ForceCreateCache") == null) {
            serveRequestFromCache(slingRequest, slingResponse, cachedElement);
            return;
        }
//no cached version availalbe
        //we build the cache for this request/component
        buildNewCacheElement(slingRequest, filterChain, slingRequest, slingResponse, cacheComponent);
        logger.debug("filter done {} {}", cacheComponent.getComponentId(), response.getBufferSize());
    }

    private void buildNewCacheElement(SlingHttpServletRequest request, FilterChain filterChain,
                                      SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse,
                                      CachedComponent newCacheComponent) throws IOException,
        ServletException {
        logger.debug("build new cache element {}", newCacheComponent.getComponentId());

        //get the parent cache component object... if this is the top level cached component in a request, this will
        // be null
        CachedComponent parentCacheComponent = getRequestCachedComponent(request);
        newCacheComponent.initializeComponent(request);

        if (parentCacheComponent != null) {
            parentCacheComponent.addChildComponent(newCacheComponent);
        } else {
            logger.info("start new cache item");
        }

        //prepare the response wrapper which will process all the child elements
        CacheServletResponseWrapper cacheResponseWrapper = new CacheServletResponseWrapper(slingResponse);
        request.setAttribute(REQ_ATTR_CACHE_COMPONENT, newCacheComponent);

        //execute the regular sling filter chain with the response Wrapper
        filterChain.doFilter(request, cacheResponseWrapper);
        //write all the data written in child components to the original response
        cacheResponseWrapper.promoteToOriginalResponse();
        newCacheComponent.done(cacheResponseWrapper);
        if (shouldCache(parentCacheComponent) && cacheResponseWrapper.getStatus() == 200) {
            logger.info("add element to cache");
            logger.info("--- put the cached value for {} in response {}", newCacheComponent.getComponentId(), 0);

            Map<String, List<String>> headerMap = cacheResponseWrapper.getHeaderMap();
            if (cacheResponseWrapper.getContentType() != null) {
                headerMap.put("Content-Type", Arrays.asList((cacheResponseWrapper.getContentType())));
            }

            if (cacheResponseWrapper.getContentLength() != null) {
                headerMap.put("Content-Length", Arrays.asList((cacheResponseWrapper.getContentLength().toString())));
            }
            if (slingRequest.getHeader("DoNotCacheJack") == null && newCacheComponent.isCacheable()) {
                cacheManagerService.put(newCacheComponent.getComponentId(), newCacheComponent.getCacheElement(),
                    newCacheComponent.getInvalidationPaths());
            }
            slingResponse.setHeader("buildthecache", "jack");
            logger.info("--- done");
        }
        if (parentCacheComponent != null) {
            //we must not set this to null as we need to have the base component in the request filter availalbe.
            // Should we put this into a separate request attr?
            request.setAttribute(REQ_ATTR_CACHE_COMPONENT, parentCacheComponent);
            parentCacheComponent.addInvalidationPaths(newCacheComponent.getInvalidationPaths());
        }
    }

    private CachedComponent getRequestCachedComponent(SlingHttpServletRequest request) {
        Object cacheComponentObj = request.getAttribute(REQ_ATTR_CACHE_COMPONENT);
        return (cacheComponentObj instanceof CachedComponent) ?
            (CachedComponent) cacheComponentObj : null;
    }

    private void serveRequestFromCache(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse,
                                       CacheElement cachedElement) throws IOException, ServletException {
        logger.info("service request from cache");
        //set cached response headers
        cachedElement.recreateResponseHeaders(slingResponse);

        //process the cached component
        cachedElement.processCacheElement(slingRequest, slingResponse, this.cacheManagerService);
        CachedComponent parentCacheComponent = getRequestCachedComponent(slingRequest);
        if (parentCacheComponent != null) {
            parentCacheComponent.addInvalidationPaths(cachedElement.getInvalidationPaths());
        }
    }

    private CachedComponent createCacheComponent(SlingHttpServletRequest slingRequest) {

        String resourcePath = getResourcePath(slingRequest.getRequestPathInfo());


        String cacheType = getCacheType(slingRequest, resourcePath);
        StringBuilder componentIdBuilder = new StringBuilder(resourcePath);
        RequestPathInfo pathInfo = slingRequest.getRequestPathInfo();
        String originalResourcePath =
            slingRequest.getResourceResolver().resolve(slingRequest.getRequestURI()).getPath();
        boolean isChildOfOriginalResource = pathInfo.getResourcePath().startsWith(originalResourcePath);
        if (!StringUtils.equals(cacheType, "fixed") && !isChildOfOriginalResource) {
            componentIdBuilder.insert(0, "--");
            componentIdBuilder.insert(0, originalResourcePath);
        }
        getResourcePath(pathInfo);
        slingRequest.getRequestParameterList().stream()
            .filter(requestParameter -> hitTheCacheJackConfig.getCacheableRequestParams().contains(requestParameter.getName()))
            .forEach(requestParameter -> componentIdBuilder.append("?").append(requestParameter.getName()).append("=").append(requestParameter.getString()));

        String componentId = componentIdBuilder.toString();

        if ("off".equals(cacheType)) {
            return new PersonalizedComponent(componentId, resourcePath, slingRequest.getResource().getResourceType(),
                hitTheCacheJackConfig);
        }
        if ("group".equals(cacheType)) {
            return new GroupComponent(componentId, resourcePath, slingRequest, cacheManagerService,
                hitTheCacheJackConfig);
        }
        if ("fixed".equals(cacheType)) {
            return new FixedComponent(componentId, resourcePath, cacheManagerService);
        }
        return new CachedComponent(componentId, resourcePath);

    }

    private String getCacheType(SlingHttpServletRequest slingRequest, String resourcePath) {
        String cacheType = slingRequest.getResource().getValueMap().get("sling:cacheType", String.class);

        logger.debug("create cache component for {} with cacheType {}", resourcePath, cacheType);
        if (StringUtils.isEmpty(cacheType)) {
            final Map<String, Object> authInfo = Collections.singletonMap(
                ResourceResolverFactory.SUBSERVICE,
                "sling-scripting");

            // Get the auto-closing Service resource resolver
            try (ResourceResolver serviceResolver = resourceResolverFactory.getServiceResourceResolver(authInfo)) {

                logger.debug("logged in as sling-scripting user to read from  {}",
                    slingRequest.getResource().getResourceType());
                Resource resourceTypeResource =
                    serviceResolver.getResource(slingRequest.getResource().getResourceType());
                if (resourceTypeResource != null) {
                    cacheType = resourceTypeResource.getValueMap().get("sling:cacheType", String.class);
                    logger.debug("read cacheType from resourceTypeResource {}: {}", resourceTypeResource.getPath(),
                        cacheType);
                }
            } catch (LoginException e) {
                logger.warn("Login Exception when obtaining a User for the Bundle Service - {}", e.getMessage());
            }
        }
        return cacheType;
    }

    private boolean shouldCache(CachedComponent parentComponent) {

        return parentComponent == null;
    }


    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }

    private String getResourcePath(RequestPathInfo pathInfo) {
        StringBuilder builder = new StringBuilder(pathInfo.getResourcePath());
        if (StringUtils.isNotEmpty(pathInfo.getSelectorString())) {
            builder.append('.');
            builder.append(pathInfo.getSelectorString());
        }
        if (StringUtils.isNotEmpty(pathInfo.getExtension())) {
            builder.append('.');
            builder.append(pathInfo.getExtension());
        }
        if (StringUtils.isNotEmpty(pathInfo.getSuffix())) {
            builder.append(pathInfo.getSuffix());
        }
        return builder.toString();
    }

    private boolean shouldHandle(SlingHttpServletRequest slingRequest) {
        if (slingRequest.getHeader("DisableCacheJack") != null) {
            return false;
        }
        if (slingRequest.getAttribute("DisableCacheJack") != null) {
            return false;
        }
        //TODO: these things are AEM specific paths. Find a good way to keep then from being cached.
        if (slingRequest.getPathInfo().contains("pulse.data.json")) {
            return false;
        }
        if (slingRequest.getPathInfo().contains("/libs/granite/security/currentuser.json")) {
            return false;
        }

        long notCacheableRequestParams = slingRequest.getRequestParameterList().stream()
            .filter(param -> !hitTheCacheJackConfig.getIgnoredRequestParams().contains(param.getName()))
            .filter(requestParameter -> !hitTheCacheJackConfig.getCacheableRequestParams().contains(requestParameter.getName())).count();
        if (notCacheableRequestParams > 0) {
            return false;
        }
        return slingRequest.getMethod().equals("GET") && hitTheCacheJackConfig.getContentTypes().get(slingRequest.getRequestPathInfo().getExtension()) != null;
    }
}
