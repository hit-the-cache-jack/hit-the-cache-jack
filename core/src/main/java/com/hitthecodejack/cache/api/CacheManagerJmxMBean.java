package com.hitthecodejack.cache.api;


public interface CacheManagerJmxMBean {

    void purgeCache();

    void clear(String path);
}
