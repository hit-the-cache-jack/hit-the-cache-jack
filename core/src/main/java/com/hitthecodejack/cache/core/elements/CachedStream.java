package com.hitthecodejack.cache.core.elements;

import com.hitthecodejack.cache.api.CacheManagerService;
import com.hitthecodejack.cache.core.filters.CacheServletResponseWrapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

public class CachedStream implements CacheElement {

    private byte[] bytes;
    private List<String> invalidationPaths;

    public CachedStream(byte[] bytes, List<String> invalidationPaths) {
        this.bytes = bytes;
        this.invalidationPaths = invalidationPaths;
    }

    @Override
    public void processCacheElement(SlingHttpServletRequest slingRequest, SlingHttpServletResponse response,
                                    CacheManagerService cacheManager) throws IOException {
        response.getOutputStream().write(this.bytes);
        response.getOutputStream().flush();
    }

    @Override
    public List<CacheElement> getChildren() {
        return Arrays.asList(this);
    }


    @Override
    public void recreateResponseHeaders(SlingHttpServletResponse response) {
        //
    }

    @Override
    public void setResponseHeaders(CacheServletResponseWrapper response) {
//
    }

    @Override
    public List<String> getInvalidationPaths() {
        return this.invalidationPaths;
    }
}
