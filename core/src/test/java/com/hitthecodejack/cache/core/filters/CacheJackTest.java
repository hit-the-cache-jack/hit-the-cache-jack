/*
 *  Copyright 2018 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.hitthecodejack.cache.core.filters;

//import org.apache.sling.testing.mock.sling.junit.SlingContext;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Rule;
import org.junit.Test;
import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;
import uk.org.lidalia.slf4jtest.TestLoggerFactoryResetRule;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class CacheJackTest {
    private static final String BASE_URL = "http://localhost";
    private static final String POST_AUTHOR = ":6502";
    private static final String POST_PUBLISH = ":6503";

    private static final String CLEAR_CACHE_JMX_URL = "/system/console/jmx/hit+the+cache+jack+cache+manager%3Aname%3DcacheManagerJmx%2Ctype%3DService/op/purgeCache/";

//    @Rule
//    public final SlingContext context = new SlingContext();

    @Rule
    public final TestLoggerFactoryResetRule testLoggerFactoryResetRule = new TestLoggerFactoryResetRule();

   // private LoggingFilter fixture = new LoggingFilter();
   // private TestLogger logger = TestLoggerFactory.getTestLogger(fixture.getClass());

    @Test
    public void testHomePage() throws IOException, ServletException {
        String url = "/content/we-retail/us/en.html";
        testUrl(url, false);
    }

    @Test
    public void testHomePagePublish() throws IOException, ServletException {
        String url = "/content/we-retail/us/en.html";
        testUrl(url, true);
    }

    @Test
    public void testCsrfToken() throws IOException, ServletException, InterruptedException {
        String url = BASE_URL + POST_AUTHOR + "/libs/granite/csrf/token.json";
        HttpResponse noCacheResponse = invokeURL(url, true, false);
        Thread.sleep(1000);
        HttpResponse cacheResponseOrig = invokeURL(url, false, false);
        Thread.sleep(1000);
        assertThat(cacheResponseOrig.getStatusLine().getStatusCode(), is(HttpStatus.OK_200));
        HttpResponse cacheResponseCached = invokeURL(url, false, false);
        assertThat(cacheResponseCached.getStatusLine().getStatusCode(), is(HttpStatus.OK_200));
        String noCacheResponseString = getResponseBody(noCacheResponse);
        String cacheOrigResponse = getResponseBody(cacheResponseOrig);
        assertThat(noCacheResponseString, is(not(cacheOrigResponse)));
        String cachedResponse = getResponseBody(cacheResponseCached);
        assertThat(noCacheResponseString, is(not(cachedResponse)));
    }

    @Test
    public void testContextHub() throws IOException, ServletException {
        String url = "/etc/cloudsettings.kernel.js/libs/settings/cloudsettings/legacy/contexthub";
        testUrl(url);
    }

    @Test
    public void testContextHubPageData() throws IOException, ServletException {
        String url = "/content/we-retail/us/en/_jcr_content/contexthub.pagedata.json";
        testUrl(url);
    }

    @Test
    public void testClientlib() throws IOException, ServletException {
        String url = "/etc.clientlibs/clientlibs/granite/utils.js";
        testUrl(url);
    }

    @Test
    public void testPreviewMode() throws IOException, ServletException {
        String url = "/content/we-retail/us/en.html";
        testUrl(url);
    }

    @Test
    public void testPublishMode() throws IOException, ServletException {
        String url = "/content/we-retail/us/en.html?wcmmode=disabled";
        testUrl(url);
    }

    @Test
    public void testEditMode() throws IOException, ServletException {
        String url = "/editor.html/content/we-retail/us/en.html";
        testUrl(url);
    }

    @Test
    public void testIncludeDispatcher() throws IOException, ServletException {
        String url = "/libs/wcm/core/content/editor/_jcr_content/content/items/content/header/items/headerbar/items/pageinfopopover/items/list/items/downloaddesign.html";
        testUrl(url);
    }

    private void testUrl(String path, boolean isPublish) throws IOException {
        clearCache(isPublish);
        String url = BASE_URL + (isPublish ? POST_PUBLISH : POST_AUTHOR) + path;
        HttpResponse noCacheResponse = invokeURL(url, true, isPublish);
        assertEquals(noCacheResponse.getStatusLine().getStatusCode(), 200);
        assertTrue(noCacheResponse.getHeaders("hitthecache").length == 0);
        HttpResponse cacheResponseOrig = invokeURL(url, false, isPublish);
        assertTrue(cacheResponseOrig.getHeaders("hitthecache").length == 0);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        HttpResponse cacheResponseCached = invokeURL(url, false, isPublish);
        assertTrue(cacheResponseCached.getHeaders("hitthecache").length == 1);
        String noCacheResponseString = removeDynamicAuthorParts(noCacheResponse);
        String origCacheResponseString = removeDynamicAuthorParts(cacheResponseOrig);
        String cachedCacheResponseString = removeDynamicAuthorParts(cacheResponseCached);
        assertEquals(noCacheResponseString, origCacheResponseString);
        assertEquals(noCacheResponseString, cachedCacheResponseString);
    }

    private String removeDynamicAuthorParts(HttpResponse cacheResponseOrig) throws IOException {
        String origCacheResponseString = getResponseBody(cacheResponseOrig).replaceAll(",\"totalTime\":\\d*,"
            + "\"selfTime\":\\d*}", "}");
        origCacheResponseString = origCacheResponseString.replaceAll("label id=\"label_[a-z,0-9,-]*\"", "label");
        origCacheResponseString = origCacheResponseString.replaceAll("(aria-)?labelledby=\"label_[a-z0-9- _]*\"", "");
        origCacheResponseString = origCacheResponseString.replaceAll("<!--cq.\"decorated\":true.*?-->", "");
        origCacheResponseString = origCacheResponseString.replaceAll("detectCachedPage\\([0-9]*\\)", "");
        origCacheResponseString = origCacheResponseString.replaceAll("policy_[0-9]*", "policy_0");
        return origCacheResponseString;
    }

    private void testUrl(String url) throws IOException {
        testUrl(url, false);
    }

    private HttpResponse invokeURL(String url, boolean disableJack, boolean isPublish) throws IOException {
        HttpGet httpGet = new HttpGet(url);
        if (disableJack) {
            httpGet.setHeader("DisableCacheJack", Boolean.TRUE.toString());
        }
        //httpGet.setHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
        CredentialsProvider provider = new BasicCredentialsProvider();
        if (!isPublish) {
//            UsernamePasswordCredentials credentials
//                    = new UsernamePasswordCredentials("admin", "admin");
//            provider.setCredentials(AuthScope.ANY, credentials);
            httpGet.setHeader("Authorization","Basic YWRtaW46YWRtaW4=");
        }
        HttpClient client = HttpClientBuilder.create()
                .setDefaultCredentialsProvider(provider)
                .build();
        HttpResponse httpGetResponse = client.execute(httpGet);
        return httpGetResponse;

    }

    private String getResponseBody(HttpResponse httpGetResponse) throws IOException {

        InputStream in = httpGetResponse.getEntity().getContent();
        return IOUtils.toString(in, "UTF-8");
    }

    private void clearCache(boolean isPublish) throws IOException {

        String url = BASE_URL + (isPublish ? POST_PUBLISH : POST_AUTHOR) + CLEAR_CACHE_JMX_URL;
        HttpPost clearCachePost = new HttpPost(url);
        clearCachePost.setHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse httpGetResponse = client.execute(clearCachePost);

    }
}
