
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class LoginSimulation extends Simulation {

	val httpProtocol = http
		.baseUrl("http://apollopoll.com")
//		.baseUrl("http://cache.hitthecodejack.com:4503")
//		.baseUrl("http://cache.hitthecodejack.com")
		.inferHtmlResources()
		.acceptHeader("*/*")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("en-US,en;q=0.5")
		.userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:62.0) Gecko/20100101 Firefox/62.0")

	val headers_1 = Map("Pragma" -> "no-cache")

	val headers_3 = Map("X-Requested-With" -> "XMLHttpRequest")

	val headers_16 = Map(
		"Accept" -> "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_20 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
		"Upgrade-Insecure-Requests" -> "1")


	val scn = scenario("LoginSimulation").repeat(3) {
		exec(http("request_6")
			.get( "/content/we-retail/us/en.html")
			.headers(headers_1)
			.resources(http("request_7")
			.get("/etc/cloudsettings.kernel.js/libs/settings/cloudsettings/legacy/contexthub"),
            http("request_11")
			.get("/content/we-retail/us/en/community/signin/_jcr_content/contexthub.pagedata.json")
			.headers(headers_3),
            http("request_12")
			.get("/content/we-retail/us/en/community/signin/_jcr_content/contexthub.commerce.orderhistory.json")
			.headers(headers_3),
            http("request_13")
			.get("/content/we-retail/us/en/community/signin/_jcr_content/contexthub.commerce.relatedproducts.json")
			.headers(headers_3),
            http("request_14")
			.post("/content/we-retail/us/en/community/signin/_jcr_content/contexthub.commerce.cart.json")
			.headers(headers_3)
			.formParam("cart", """{"ignorePost":true}"""),
            http("request_15")
			.get("/content/we-retail/us/en/community/signin/_jcr_content/contexthub.commerce.smartlists.json")
			.headers(headers_3),
            http("request_16")
			.get("/etc/segmentation.segment.js?_=1539698589055")
			.headers(headers_16),
            http("request_17")
			.get("/etc/clientcontext/default/content/jcr:content/stores.init.js?path=%2Fcontent%2Fwe-retail%2Fus%2Fen%2Fcommunity%2Fsignin&_=1539698589056")
			.headers(headers_16)))
		.pause(1)
		.exec(http("request_19")
			.get("/libs/granite/csrf/token.json")
			.resources(http("request_20")
			.post("/content/we-retail/us/en/community/signin/j_security_check")
			.headers(headers_20)
			.formParam("resource", "/content/we-retail/us/en/experience/fly-fishing-the-amazon.html")
			.formParam("_charset_", "UTF-8")
			.formParam("j_username", "gatling")
			.formParam("j_password", "gatling"),
            http("request_21")
			.get("/etc/cloudsettings.kernel.js/libs/settings/cloudsettings/legacy/contexthub"),
            http("request_25")
			.get("/content/we-retail/us/en/experience/fly-fishing-the-amazon/_jcr_content/contexthub.commerce.orderhistory.json")
			.headers(headers_3),
            http("request_27")
			.get("/content/we-retail/us/en/experience/fly-fishing-the-amazon/_jcr_content/contexthub.pagedata.json")
			.headers(headers_3),
            http("request_28")
			.post("/content/we-retail/us/en/experience/fly-fishing-the-amazon/_jcr_content/contexthub.commerce.cart.json")
			.headers(headers_3)
			.formParam("cart", """{"ignorePost":true}"""),
            http("request_29")
			.get("/content/we-retail/us/en/experience/fly-fishing-the-amazon/_jcr_content/contexthub.commerce.smartlists.json")
			.headers(headers_3),
            http("request_30")
			.get("/content/we-retail/us/en/experience/fly-fishing-the-amazon/_jcr_content/contexthub.commerce.relatedproducts.json")
			.headers(headers_3),
            http("request_31")
			.get("/content/we-retail/us/en/experience/fly-fishing-the-amazon/_jcr_content/contexthub.commerce.orderhistory.json")
			.headers(headers_3))
			.check(status.is(200)))
		.pause(3)
		.exec(http("request_32")
			.get("/content/we-retail/us/en/women.html")
			.headers(headers_20)
			.resources(http("request_33")
			.get("/etc/cloudsettings.kernel.js/libs/settings/cloudsettings/legacy/contexthub"),
            http("request_37")
			.get("/content/we-retail/us/en/women/_jcr_content/contexthub.pagedata.json")
			.headers(headers_3),
            http("request_38")
			.get("/content/we-retail/us/en/women/_jcr_content/contexthub.commerce.orderhistory.json")
			.headers(headers_3),
            http("request_39")
			.post("/content/we-retail/us/en/women/_jcr_content/contexthub.commerce.cart.json")
			.headers(headers_3)
			.formParam("cart", """{"ignorePost":true}"""),
            http("request_40")
			.get("/content/we-retail/us/en/women/_jcr_content/contexthub.commerce.smartlists.json")
			.headers(headers_3),
            http("request_42")
			.get("/content/we-retail/us/en/women/_jcr_content/contexthub.commerce.relatedproducts.json")
			.headers(headers_3)))
		.pause(4)
		.exec(http("request_43")
			.get("/content/we-retail/us/en/products/equipment/hiking.html")
			.headers(headers_20)
			.resources(http("request_44")
			.get("/etc/cloudsettings.kernel.js/libs/settings/cloudsettings/legacy/contexthub"),
            http("request_48")
			.get("/content/we-retail/us/en/products/equipment/hiking/_jcr_content/contexthub.pagedata.json")
			.headers(headers_3)))
		.pause(3)
		.exec(http("request_50")
			.get("/content/we-retail/us/en/products/equipment/surfing/pipeline-board-shorts.html")
			.headers(headers_20)
			.resources(http("request_51")
			.get("/etc/cloudsettings.kernel.js/libs/settings/cloudsettings/legacy/contexthub"),
            http("request_54")
			.get("/content/we-retail/us/en/products/equipment/surfing/pipeline-board-shorts/_jcr_content/contexthub.commerce.orderhistory.json")
			.headers(headers_3),
            http("request_56")
			.get("/content/we-retail/us/en/products/equipment/surfing/pipeline-board-shorts/_jcr_content/contexthub.pagedata.json")
			.headers(headers_3),
            http("request_57")
			.post("/content/we-retail/us/en/products/equipment/surfing/pipeline-board-shorts/_jcr_content/contexthub.commerce.cart.json")
			.headers(headers_3)
			.formParam("cart", """{"ignorePost":true}"""),
            http("request_58")
			.get("/content/we-retail/us/en/products/equipment/surfing/pipeline-board-shorts/_jcr_content/contexthub.commerce.smartlists.json")
			.headers(headers_3),
            http("request_61")
			.get("/services/social/templates?resourceType=weretail/components/hbs/rating&ext=hbs&selector=")
			.headers(headers_3)
			.check(status.is(404)),
            http("request_62")
			.get("/content/we-retail/us/en/products/equipment/surfing/pipeline-board-shorts/_jcr_content/contexthub.commerce.relatedproducts.json")
			.headers(headers_3),
            http("request_63")
			.get("/services/social/templates?resourceType=social/reviews/components/hbs/reviews&ext=hbs&selector=")
			.headers(headers_3)
			.check(status.is(404))))
    }

    setUp(scn.inject(rampUsers(10) during (5 seconds))).protocols(httpProtocol)
}
