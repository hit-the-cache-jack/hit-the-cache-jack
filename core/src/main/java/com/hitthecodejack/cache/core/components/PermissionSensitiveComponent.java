package com.hitthecodejack.cache.core.components;

import com.hitthecodejack.cache.api.RequestAttributeCacheTransformer;
import com.hitthecodejack.cache.api.config.HitTheCacheJackConfig;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class PermissionSensitiveComponent extends CachedComponent {

    private static final int SESSION_SPECIFIC_OBJECT_CHECK_RECURSION_DEPTH = 2;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    protected String resourceToInclude;
    String resourceType;
    private HitTheCacheJackConfig config;
    private List<RequestAttributeCacheTransformer> requestAttributeTransformers = new ArrayList<>();

    public PermissionSensitiveComponent(String pathBasedId, String resourceToInclude, String resourceType, HitTheCacheJackConfig config) {
        super(pathBasedId, resourceToInclude);
        this.resourceType = resourceType;

        this.config = config;
        try {
            BundleContext bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();

            for (ServiceReference<RequestAttributeCacheTransformer> reference :
                ((ServiceReference<RequestAttributeCacheTransformer>[]) bundleContext.getAllServiceReferences(RequestAttributeCacheTransformer.class.getName(), null))) {
                requestAttributeTransformers.add(bundleContext.getService(reference));
            }
        } catch (Exception e) {
            logger.warn("Failed to load requestAttributeTransformer implementations: {}", e, e);
        }
        this.resourceToInclude = resourceToInclude;
    }

    protected Map<String, Object> getCacheableRequestAttributes(SlingHttpServletRequest request) {
        long start = System.currentTimeMillis();

        Map<String, Object> storedRequestAttributes = new HashMap<>();
        Enumeration<String> attributeNames = request.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String attributeName = attributeNames.nextElement();
            Object cachableRequestAttribute = processAttribute(request, storedRequestAttributes, attributeName);
            if (cachableRequestAttribute != null) {
                storedRequestAttributes.put(attributeName, cachableRequestAttribute);
            }
        }
        long duration = System.currentTimeMillis() - start;
        logger.info("getting cacheable request attributes took {}", duration);

        return storedRequestAttributes;
    }

    private Object processAttribute(SlingHttpServletRequest request, Map<String, Object> storedRequestAttributes,
                                    String attributeName) {
        Object attribute = request.getAttribute(attributeName);
        //check if there is a transformer for this attribute
        for (RequestAttributeCacheTransformer requestAttributeCacheTransformer : requestAttributeTransformers) {
            Object transformedAttribute =
                requestAttributeCacheTransformer.createCachableObject(request.getAttribute(attributeName));
            if (transformedAttribute != null) {
                return transformedAttribute;
            }
        }

        if (!config.getIgnoreRequestAttributes().contains(attributeName)) {
            if (!isSessionSpecificObject(attribute, attribute.getClass(), 0)) {
                logger.debug("Request attribute not linked with resource : {}", attributeName);
                return attribute;
            } else {
                logger.info("session specific request attribute will not be cached:{}", attributeName);
            }
        }
        //request attribute is either excluded or session specific
        return null;
    }

    private boolean isSessionSpecificObject(Object object, Class type, int depth) {
        if (depth > SESSION_SPECIFIC_OBJECT_CHECK_RECURSION_DEPTH) {
            //circular references are possible, we need to put robustness above perfection
            return false;
        }
        logger.debug("check session specific object {}", type.getCanonicalName());
        if (type.isPrimitive() || type.isAssignableFrom(String.class)) {
            logger.debug("object is primitive");
            return false;
        }
        if (type.isAssignableFrom(Resource.class) || type.isAssignableFrom(ResourceResolver.class) || type.isAssignableFrom(Session.class) || type.isAssignableFrom(SlingHttpServletRequest.class) || type.isAssignableFrom(SlingHttpServletResponse.class)) {
            logger.debug("object is session specific");
            return true;
        }
        if (isMapWithSessionSpecificValue(object, depth + 1)) {
            return true;
        }
        for (Field field : FieldUtils.getAllFields(type)) {
            try {
                if (!Modifier.isStatic(field.getModifiers())) {
                    Object fieldObject = FieldUtils.readDeclaredField(object, field.getName(), true);
                    if (fieldObject != null && isSessionSpecificObject(fieldObject, field.getType(), depth + 1)) {
                        logger.debug("object is session specific because it contains session specific fields: {}",
                            type.getCanonicalName());
                        return true;
                    }
                }
            } catch (Exception e) {
                logger.debug("Failed to read field {}", field.getName());
            }

        }
        logger.debug("object is not session specific {}", type.getCanonicalName());
        return false;
    }

    private boolean isMapWithSessionSpecificValue(Object attribute, int depth) {
        try {
            if (attribute instanceof Map) {
                for (Object value : ((Map) attribute).values()) {
                    if (isSessionSpecificObject(value, value.getClass(), depth)) {
                        return true;
                    }
                }
            }
        } catch (UnsupportedOperationException e) {
            logger.debug("unable to check {}", attribute);
        }
        return false;
    }

    @Override
    public boolean canBeAgreggated() {
        return false;
    }
}
