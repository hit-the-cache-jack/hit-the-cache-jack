package com.hitthecodejack.cache.api.impl;

import com.hitthecodejack.cache.api.CacheManagerService;
import com.hitthecodejack.cache.core.elements.CacheElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.infinispan.Cache;
import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = CacheManagerService.class, property = {"process.label=Cache Manager Service"}, immediate = true)
@Designate(ocd = CacheManagerServiceImpl.OsgiConfiguration.class)
public class CacheManagerServiceImpl implements CacheManagerService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static Cache<String, CacheElement> cache = null;
    private Map<String, List<String>> invalidationMap = new HashMap<>();

    private int statsFilesLevel;

    @Activate
    public void activate(OsgiConfiguration config) {
        statsFilesLevel = config.htcj_statsfileslevel();
        if (cache == null) {
            GlobalConfiguration g = new GlobalConfigurationBuilder()
                .globalJmxStatistics()
                .allowDuplicateDomains(true)
                .build();

            Configuration cfg = new ConfigurationBuilder()
                .build();
            cache = new DefaultCacheManager(g, cfg).getCache();
        } else {
            cache.clear();
        }
    }


    @ObjectClassDefinition(name = "Config Service")
    public @interface OsgiConfiguration {
        @AttributeDefinition(
            name = "statsfileslevel",
            description = "cache invalidation level",
            type = AttributeType.INTEGER
        )
        int htcj_statsfileslevel() default 2;

    }

    @Override
    public CacheElement get(String key) {
        return cache.get(key);
    }

    @Override
    public void put(String key, CacheElement component, List<String> invalidationPaths) {
        cache.put(key, component);
        for (String invalidationPath : invalidationPaths) {
            String evictionKey = getEvictionKey(invalidationPath);
            List<String> evictionList = this.invalidationMap.get(evictionKey);
            if (evictionList == null) {
                evictionList = new ArrayList<>();
                invalidationMap.put(evictionKey, evictionList);
            }
            if (!evictionList.contains(key)) {
                evictionList.add(key);
            }
        }

    }

    @Override
    public void purge() {
        cache.clear();
    }

    @Override
    public void clear(String path) {
        String evictionKey = getEvictionKey(path);
        List<String> evictionList = this.invalidationMap.remove(evictionKey);
        if (evictionList != null) {
            evictionList.forEach(key -> cache.evict(key));
        }
    }

    private String getEvictionKey(String path) {
        int endIndex = 0;
        for (int i = 0; i <= statsFilesLevel; i++) {
            endIndex = path.indexOf("/", endIndex + 1);
            if (endIndex == -1) {
                return path;
            }
        }
        return path.substring(0, endIndex);
    }

}
