package com.hitthecodejack.cache.core.elements;

import com.day.cq.wcm.api.components.IncludeOptions;
import com.hitthecodejack.cache.api.CacheManagerService;
import com.hitthecodejack.cache.api.CachedTransformedRequestAttribute;
import com.hitthecodejack.cache.core.filters.CacheServletResponseWrapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestDispatcherOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonalizedCacheElement extends AbstractCacheElement implements CacheElement {


    private Map<String, Object> requestAttributes;
    protected String resourceType;
    private IncludeOptions origIncludeOptions;
    protected String path;


    private final Logger logger = LoggerFactory.getLogger(getClass());

    public PersonalizedCacheElement(String elementId, String path, String resourceType, CacheServletResponseWrapper response,
                                    IncludeOptions options,
                                    Map<String, Object> cacheableRequestAttributes, List<String> invalidationPaths) {
        super(elementId, response, invalidationPaths);
        this.path = path;
        this.origIncludeOptions = options;
        this.requestAttributes = cacheableRequestAttributes;
        this.resourceType = resourceType;
    }

    @Override
    public void processCacheElement(SlingHttpServletRequest slingRequest, SlingHttpServletResponse response,
                                    CacheManagerService cacheManager) throws IOException, ServletException {

        RequestDispatcherOptions requestDispatcherOptions = this.recreateRequestAttributes(slingRequest,
            this.requestAttributes);
        slingRequest.getRequestDispatcher(this.path, requestDispatcherOptions).include(slingRequest, response);
    }

    @Override
    public List<CacheElement> getChildren() {
        return new ArrayList<>();
    }


    private RequestDispatcherOptions recreateRequestAttributes(SlingHttpServletRequest slingRequest,
                                                               Map<String, Object> requestAttributes) {

        //TODO: IncludeOptions are AEm specific, handle this case with the CachedTransformedRequestAttribute interface
        IncludeOptions includeOptions = IncludeOptions.getOptions(slingRequest, true);
        includeOptions.getCssClassNames().addAll(origIncludeOptions.getCssClassNames());
        includeOptions.setDecorationTagName(origIncludeOptions.getDecorationTagName());

        for (String attributeName : requestAttributes.keySet()) {
            processAttribute(slingRequest, requestAttributes, attributeName);
        }
        RequestDispatcherOptions requestDispatcherOptions = new RequestDispatcherOptions();
        if (StringUtils.isNotEmpty(resourceType)) {
            requestDispatcherOptions.setForceResourceType(resourceType);
        }
        return requestDispatcherOptions;
    }

    private void processAttribute(SlingHttpServletRequest slingRequest, Map<String, Object> requestAttributes,
                                  String attributeName) {
        Object cachedRequestAttribute = requestAttributes.get(attributeName);
        if (cachedRequestAttribute instanceof CachedTransformedRequestAttribute) {
            slingRequest.setAttribute(attributeName,
                ((CachedTransformedRequestAttribute) cachedRequestAttribute).recreateRequestAttribute(slingRequest));
            return;
        }
        if (slingRequest.getAttribute(attributeName) == null) {
            slingRequest.setAttribute(attributeName, cachedRequestAttribute);
        } else {
            //TODO: some attributes must not be overwritten (e.g. tokens), while other may contain critical
            // updated data
            //we should add a config for attributes that must be overwritten
            logger.info("request attribute {} exists, do not overwrite", attributeName);
        }
    }
}
